
# MDCalc Dev Exercise

  

### This page is visible at: [MDCalc](https://mdcalc.dasu.io/)

  

## Architecture Considerations

I opted to install the following packages for this challenge:

- TypeScript

  - for type safety — helps to prevent the client from seeing undefined or null values in production

- emotion/styled

  - CSS-in-JS library which allows for inline styles with an API resembling styled-components; familiar workflow for React Native developers

- redux/redux-toolkit

  - state management, toolkit gave some convenience functions which guarantee immutable updates since it uses Immer internally

- axios

  - to fetch the patient data

- react-hook-form

  - helps provide an elegant interface to permit form validation

  - redux form and formik both trigger unnecessary re-renders onChangeEvent which I have noticed in production in various applications; this seems to alleviate this problem

  - another contender was [GitHub - final-form/react-final-form: 🏁 High performance subscription-based form state management for React](https://github.com/final-form/react-final-form)

- yup

  - allows for form validation with some helpers as well as the ability to provide a schema which seems to be inline with the TypeScript mindset

## The Challenge

1. Design -  [MDCalc](https://mdcalc.dasu.io/)
1. Hosting - hosted on HTTP and redirecting to HTTPS. Additionally since this create-react-app has a service worker config setup out of the box it is also setup as a PWA (progressive web app). This could be useful for doctors to have since hospitals can have spotty network connectivity in areas

1. Fetch Data is being done with axios and stored in redux

1. Form editing is enabled with react-hook-form

1. Calculation is done in App.js [Bitbucket](https://bitbucket.org/dreamstack/react-mdcalc/src/2ba0afac97e867792426787abadf81473526dac4/src/App.js#lines-38)

1. Results are rendered in the div underneath the Styled Calculator

1. Extra info tabs: TODO

   - Although I ran out of time to complete this, I will be able to modify the <ButtonGroup/> component to take a `selected` property that determines which angle to rotate the chevron icon contained within each button, as well as conditionally color the button to its `active` state. I left this off for the very end because I’ve been trying to think of a clever algorithm in my head for calculating the position of the callout bubble tail pointing to its parent button.

  

## Caveats/Lessons Learned

I opted to create custom UI components as opposed to reusing a library such as react-material-ui and may have had to sacrifice some details due to the time invested in creating these components. I think for the sake of delivering on the breadth of features I would have been better off opting for a UI library of some kind; but this does come at a cost as well as they all have a learning curve of their own. I have used react-material-ui in the past and felt it to have just as much of a learning curve as styled-components or emotion. I feel that creating custom UI components is really not too complicated with the functional composition that React provides us; and that this time investment pays off when we then want to interface with other libraries, such as form libraries, animation libraries, etc.