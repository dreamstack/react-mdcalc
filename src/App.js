import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import { useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { unwrapResult } from "@reduxjs/toolkit";
import { get } from "lodash";
import {
  fetchSexAndAgeInfo,
  fetchHeightInfo,
  fetchWeightInfo,
} from "./actions";
import {
  Button,
  ButtonGroup,
  TextInput,
  Calculator,
  CalcField,
  ChevronDirection,
} from "./components/";
import * as yup from "yup";
import { theme } from "./public";

const schema = yup.object().shape({
  gender: yup.string(),
  age: yup.number().integer(),
  weight: yup.number(),
  height: yup.number(),
  creatinine: yup.number(),
});

function App() {
  const dispatch = useDispatch();
  const [gender, setGender] = useState("");
  const [results, setResults] = useState(0);
  const [severity, setSeverity] = useState("");
  const [viewPerils, setViewPerils] = useState(false);
  const [viewWhenToUse, setViewWhenToUse] = useState(false);
  const { control, handleSubmit, setValue } = useForm({
    validationSchema: schema,
  });

  const calculate = ({ gender, creatinine, height, weight, age }) => {
    const genderPts = gender === "male";
    const agePts = age > 40;
    const weightPts = weight > 60;
    const heightPts = height > 160;
    const creatininePts = creatinine != undefined && creatinine > 0.7;
    let totalPts =
      0 + genderPts + agePts + weightPts + heightPts + creatininePts;
    const sevString = totalPts > 3 ? "high" : "low";
    setSeverity(sevString);
    setResults(totalPts);
  };

  const calculateAge = (birthday) => {
    const birthdate = new Date(birthday);
    const ageDifMs = Date.now() - birthdate.getTime();
    const ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  };

  /**
   * responsible for firing our three separate API calls
   * which will GET the data,
   * we will then process it to be used in local form state
   */
  const fetchPatientData = async () => {
    try {
      const patients = await dispatch(fetchSexAndAgeInfo());
      const weights = await dispatch(fetchWeightInfo());
      const heights = await dispatch(fetchHeightInfo());
      const patientSexAndAge = unwrapResult(patients);
      const patientWeight = unwrapResult(weights);
      //unsafely unwrapping for the sake of time
      const formatWeight =
        patientWeight["entry"][0]["resource"]["valueQuantity"]["value"];
      setValue("weight", formatWeight);
      const patientHeight = unwrapResult(heights);
      const formatHeight =
        patientHeight["entry"][0]["resource"]["valueQuantity"]["value"];
      setValue("height", formatHeight);
      setGender(patientSexAndAge["gender"]);
      setValue("gender", patientSexAndAge["gender"]);
      const age = calculateAge(patientSexAndAge["birthDate"]);
      setValue("age", age);
      const weightValue = get(patientWeight, "value", 0);
      setValue("kg", weightValue);
      const heightValue = get(patientHeight, "value", 0);
      setValue("cm", heightValue);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchPatientData();
  }, []);

  const headerButtons = [
    {
      label: "When to Use",
      onClick: () => {
        setViewPerils(false);
        setViewWhenToUse(!viewWhenToUse);
      },
      fullyRounded: false,
      active: viewWhenToUse,
      chevron: ChevronDirection.Up,
    },
    {
      label: "Perils & Pitfalls",
      onClick: () => {
        setViewWhenToUse(false);
        setViewPerils(!viewPerils);
      },
      fullyRounded: false,
      active: viewPerils,
      chevron: ChevronDirection.Up,
    },
  ];

  const buttons = [
    {
      label: "Female",
      onClick: () => {
        setValue("gender", "female");
        setGender("female");
      },
      fullyRounded: false,
      active: gender === "female",
      chevron: ChevronDirection.None,
    },
    {
      label: "Male",
      onClick: () => {
        setValue("gender", "male");
        setGender("male");
      },
      fullyRounded: false,
      active: gender === "male",
      chevron: ChevronDirection.None,
    },
  ];

  return (
    <StyledContainer>
      <ExtraInfo>
        <ButtonGroup buttons={headerButtons} />
      </ExtraInfo>
      {viewWhenToUse && (
        <ExtraInfoCard>
          <ul>
            {extraInfoJson.whenToUse.map((time) => (
              <li>{time}</li>
            ))}
          </ul>
        </ExtraInfoCard>
      )}

      {viewPerils && (
        <ExtraInfoCard>
          <>
            <b>{extraInfoJson.perilsPitfalls.title}</b>
            <p>{extraInfoJson.perilsPitfalls.text}</p>
          </>
        </ExtraInfoCard>
      )}

      <Calculator>
        <form onSubmit={handleSubmit(calculate)}>
          <CalcField label="Sex">
            {" "}
            <Controller
              as={
                <ButtonGroup
                  buttons={buttons}
                  chevron={ChevronDirection.None}
                />
              }
              name={"gender"}
              defaultValue={""}
              control={control}
            />
          </CalcField>
          <CalcField label="Age">
            {" "}
            <Controller
              as={<TextInput label="years" />}
              name={"age"}
              defaultValue={0}
              control={control}
            />
          </CalcField>
          <CalcField label="Weight">
            {" "}
            <Controller
              as={<TextInput label="kg" />}
              name={"weight"}
              defaultValue={0}
              control={control}
            />
          </CalcField>
          <CalcField label="Creatinine">
            {" "}
            <Controller
              as={<TextInput label="mg/dL" />}
              name={"creatinine"}
              control={control}
            />
          </CalcField>
          <CalcField label="Height">
            {" "}
            <Controller
              as={<TextInput label="cm" />}
              name={"height"}
              defaultValue={0}
              control={control}
            />
          </CalcField>
          <Button label="Calculate" onClick={handleSubmit} />
        </form>
      </Calculator>
      <div>
        <h4>Results</h4>
        <h2>{results}</h2>
        <h4>Severity</h4>
        <h2>{severity}</h2>
      </div>
    </StyledContainer>
  );
}

const StyledContainer = styled.div`
  display: flex;
  flex-grow: 1;
  padding: 50px;
  flex-direction: column;
  justify-content: space-between;
`;

const ExtraInfo = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  align-content: center;
  align-items: center;
  padding-bottom: 20px;
`;

const extraInfoJson = {
  whenToUse: [
    "Assessing a patient’s renal function",
    "Prescribing a drug that is renally metabolized",
  ],
  perilsPitfalls: {
    title: `From Dan Brown, PharmD, at Palm Beach Atlantic University, the
  primary author of the functional range of creatinine clearance paper:`,
    text: `The Cockcroft-Gault equation remains the gold standard after almost
  40 years, despite inaccuracies that arise from variations in body composition among
  patients. Those who understand potential sources of error can adjust accordingly.`,
  },
};

const ExtraInfoCard = styled.div`
  flex-grow: 1;
  text-align: left;
  flex-wrap: wrap;
  max-width: 1000px;
  background-color: ${theme.offWhite};
`;

export default App;
