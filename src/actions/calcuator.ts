import { createAsyncThunk } from "@reduxjs/toolkit";
import { CalculatorService } from "../services";

// First, create the thunk
export const fetchSexAndAgeInfo = createAsyncThunk(
  "fetchSexAndAgeInfo",
  async () => {
    const response = await CalculatorService.getSexAndAgeInfo();
    return response;
  }
);

export const fetchWeightInfo = createAsyncThunk("fetchWeightInfo", async () => {
  const response = await CalculatorService.getWeightInfo();
  return response;
});

export const fetchHeightInfo = createAsyncThunk("fetchHeightInfo", async () => {
  const response = await CalculatorService.getHeightInfo();
  console.log(response);
  return response;
});
