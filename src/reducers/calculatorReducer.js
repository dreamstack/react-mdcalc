import { createSlice } from "@reduxjs/toolkit";
import {
  fetchSexAndAgeInfo,
  fetchHeightInfo,
  fetchWeightInfo,
} from "../actions";

// type PatientData = {
//   id: string;
//   resourceType: string;
//   birthDate: string;
//   active: boolean;
//   gender: string;
//   deceasedBoolean: boolean;
//   //any just to save time for now
//   careProvider: Array<any>;
//   name: Array<any>;
//   identifier: Array<any>;
//   address: Array<any>;
//   maritalStatus: Array<any>;
//   communication: Array<any>;
//   extension: Array<any>;
// };

const initialState = {
  entities: [],
  loading: "idle",
  currentRequestId: undefined,
  error: null,
};

export const sexAndAge = createSlice({
  name: "sexAndAge",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchSexAndAgeInfo.fulfilled, (state, action) => {
      state.entities.push(action.payload);
    });
  },
}).reducer;

export const height = createSlice({
  name: "height",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchHeightInfo.fulfilled, (state, action) => {
      state.entities.push(action.payload);
    });
  },
}).reducer;

export const weight = createSlice({
  name: "weight",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchWeightInfo.fulfilled, (state, action) => {
      state.entities.push(action.payload);
    });
  },
}).reducer;
