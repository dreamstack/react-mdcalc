import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import store from "./app/store";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import { Global, css } from "@emotion/core";
import emotionNormalize from "emotion-normalize";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Global
        styles={css`
          ${emotionNormalize}
        `}
      />
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
