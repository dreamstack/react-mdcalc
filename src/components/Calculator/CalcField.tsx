import React, { FunctionComponent } from "react";
import styled from "@emotion/styled";
import { theme } from "../../public";

// const breakpoints = [300, 576, 768, 992, 1200];

// const mq = breakpoints.map((bp) => `@media (min-width: ${bp}px)`);
/**
 * children can be one of:
 * button group or text input
 */
interface IStyledCalcFieldProps {
  label?: string;
  //   onChangeValue: () => void;
}

export const CalcField: FunctionComponent<IStyledCalcFieldProps> = (props) => (
  <StyledCalcField>
    <StyledLabel>{props.label}</StyledLabel>
    <StyledChildren>{props.children}</StyledChildren>
  </StyledCalcField>
);

const StyledCalcField = styled.div(() => ({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  // [mq[0]]: { maxWidth: "400px" },
  // [mq[1]]: { maxWidth: "500px" },
  // [mq[2]]: { maxWidth: "600px" },
  // [mq[3]]: { maxWidth: "700px" },
  // [mq[4]]: { maxWidth: "800px" },
  justifyContent: "space-between",
  borderTop: `1px solid ${theme.lightGrey}`,
  borderBottom: `1px solid ${theme.lightGrey}`,
  lastChild: {
    borderBottom: `1px solid ${theme.lightGrey}`,
  },
}));

const StyledLabel = styled.p`
  line-height: 2;
  font-size: 1.2rem;
  font-weight: normal;
  text-align: left;
  flex-grow: 0.5;
  border: none;
  border-radius: 5px;
  align-self: flex-start;
`;

const StyledChildren = styled.div`
  flex-shrink: 0.4;
  text-align: right;
  align-self: center;
`;
