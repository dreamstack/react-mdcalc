import React, { FunctionComponent } from "react";
import styled from "@emotion/styled";

interface IStyledCalculatorProps {
  label?: string;
}

export const Calculator: FunctionComponent<IStyledCalculatorProps> = (
  props
) => <StyledCalculator>{props.children}</StyledCalculator>;

const StyledCalculator = styled.form`
  flex-grow: 1;
  flex-direction: column;
  justify-content: space-evenly;
`;
