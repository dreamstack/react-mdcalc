import React, { RefObject } from "react";
import styled from "@emotion/styled";
import { theme } from "../../public";

const breakpoints = [300, 576, 768, 992, 1200];

const mq = breakpoints.map((bp) => `@media (min-width: ${bp}px)`);

interface IStyledTextInputProps {
  label?: string;
  onChangeValue: () => void;
  ref:
    | string
    | ((instance: HTMLInputElement | null) => void)
    | RefObject<HTMLInputElement>
    | null
    | undefined;
  defaultValue: string;
}

export const TextInput = ({
  label = "value",
  ref,
  ...rest
}: IStyledTextInputProps) => {
  return (
    <StyledTextInput>
      <input {...rest} ref={ref} />
      <button>{label}</button>
    </StyledTextInput>
  );
};

const StyledTextInput = styled.div(() => ({
  display: "flex",
  flexGrow: 1,
  flexDirection: "row",
  border: `1px solid ${theme.lightGrey}`,
  borderRadius: "5px",
  [mq[0]]: { minWidth: "300px" },
  [mq[1]]: { minWidth: "300px" },
  [mq[2]]: { minWidth: "400px" },
  [mq[3]]: { minWidth: "500px" },
  [mq[4]]: { minWidth: "600px" },
  input: {
    alignSelf: "stretch",
    lineHeight: 2,
    fontSize: "1.2rem",
    fontWQight: "normal",
    textAlign: "left",
    flexGrow: 1,
    border: "none",
    borderRadius: "5px",
  },
  button: {
    background: theme.lightGrey,
    borderTopRightRadius: "4px",
    borderBottomRightRadius: "4px",
    borderRight: "none",
    borderTop: "none",
    borderBottom: "none",
  },
}));
