import React from "react";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import { theme } from "../../public";

export enum ChevronDirection {
  Up = "up",
  Down = "down",
  None = "none",
}

export interface IButtonProps {
  onClick: () => void;
  active: boolean;
  chevron: ChevronDirection;
  label?: string;
  fullyRounded?: boolean;
}

export const Button = ({
  label = "Click Here",
  fullyRounded = true,
  active = false,
  onClick,
  chevron = ChevronDirection.None,
}: IButtonProps) => {
  const chevronClass = `chevron ${chevron}`;
  return (
    <StyledButton
      active={active}
      fullyRounded={fullyRounded}
      onClick={onClick}
      chevron={chevron}
    >
      {label}
      {chevron !== ChevronDirection.None && (
        <span className={chevronClass}></span>
      )}
    </StyledButton>
  );
};

const dynamicStyle = (props: IButtonProps) =>
  css`
    background-color: ${props.active ? theme.green : theme.lightGrey};
    color: ${props.active ? "white" : "black"};
    border-radius: ${props.fullyRounded ? "5px" : "0px"};
  `;

const StyledButton = styled.button`
  text-align: center;
  font-size: 24px;
  margin-top: 10px;
  padding-top: 5px;
  padding-bottom: 5px;
  border: 0px;
  max-width: 150px;
  .chevron::before {
    border-style: solid;
    margin-left: 20px;
    border-width: 0.15em 0.15em 0 0;
    content: "";
    color: ${theme.grey};
    display: inline-block;
    height: 0.6em;
    transform: rotate(135deg);
    width: 0.6em;
  }

  .chevron.right:before {
    left: 10;
    right: 5;
    transform: rotate(45deg);
  }
  ${dynamicStyle}
`;
