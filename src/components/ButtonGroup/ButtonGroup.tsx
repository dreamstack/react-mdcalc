import React from "react";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import { Button, IButtonProps, ChevronDirection } from "../Button/Button";

interface IButtonGroupProps {
  buttons: Array<IButtonProps>;
  chevron: ChevronDirection;
}

export const ButtonGroup = (props: IButtonGroupProps) => (
  <StyledButtonGroup chevron={props.chevron} buttons={props.buttons}>
    {props.buttons.map((button) => (
      <Button key={button.label} {...button} />
    ))}
  </StyledButtonGroup>
);

const dynamicStyle = (props: IButtonGroupProps) =>
  css`
    min-width: ${props.chevron !== ChevronDirection.None && `300px`};
    max-width: ${props.chevron !== ChevronDirection.None && `1000px`};
  `;

const StyledButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  button {
    flex-grow: 1;
    display: flex;
    justify-content: center;
    text-align: center;
    align-content: stretch;
    line-height: 2;
    height: 50px;
    font-size: 1.2rem;
    font-weight: normal;
    text-align: center;
    padding: 5px 50px 5px 50px;
    :first-child {
      border-top-left-radius: 5px;
      border-bottom-left-radius: 5px;
    }
    :last-child {
      border-top-right-radius: 5px;
      border-bottom-right-radius: 5px;
    }
    ${dynamicStyle}
  }
`;
