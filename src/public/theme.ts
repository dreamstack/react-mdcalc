export const theme = {
  red: "#FF0000",
  black: "#111111",
  darkGrey: "#3F3F3F",
  grey: "#3A3A3A",
  lightGrey: "#E1E1E1",
  green: "#1bb193",
  darkGreen: "#117d67",
  offWhite: "#CACACA",
  white: "#FFFFFF",
  blueGrey: "#b0c4de",
  maxWidth: "1000px",
  bs: "0 12px 24px 0 rgba(0, 0, 0, 0.09)",
};
