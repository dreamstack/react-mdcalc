import axios from "axios";

// const handleResponse = (responseData: any) => {
//   const { data, status, statusText } = responseData;
//   if (200 <= status < 300) {
//     return data;
//   } else {
//     const errors = { error: status, errorText: statusText };
//     return Promise.reject(errors);
//   }
// };

export class CalculatorService {
  static getSexAndAgeInfo = async () => {
    const endpoint: string =
      "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB";
    const { data } = await axios.get(endpoint);
    return data;
  };

  static getWeightInfo = async () => {
    const endpoint =
      "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB&code=29463-7";
    const { data } = await axios.get(endpoint);
    return data;
  };

  static getHeightInfo = async () => {
    const endpoint =
      "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB&code=8302-2";
    const { data } = await axios.get(endpoint);
    return data;
  };
}
