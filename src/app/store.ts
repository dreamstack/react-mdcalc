import { configureStore } from "@reduxjs/toolkit";

import { sexAndAge, height, weight } from "../reducers";

export default configureStore({
  reducer: {
    sexAndAge,
    height,
    weight,
  },
});
